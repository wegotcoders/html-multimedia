# Image Slider #

Using HTML and external css and javascript to create an image slider.

### Clone Repository ###

1. Fork this repository.
2. Git clone your own copy to your assignments folder.
3. Change directory into your local copy of the repo.

### To Do ###

1. Open image_slider.html in Sublime, notice the script and link elements in the head, they are there to bring in external resources.
2. Get some of your favourite photos and put them in the images sub-folder of your local repo folder.
3. Remove list items, add list items to image_slider.html, add image elements, each src refers to each image in your images folder.
4. You can see your image slider by typing in terminal 'open image_slider.html'.